package com.itb.menu;

import com.itb.dades.InputData;
import com.itb.recursivitat.RecursivityMethods;

import java.util.Scanner;

/**
 * @author Ricardo Montserrat
 * @version 2.5
 * Clase principal de prueba para Recursividad.
 */
public class TryingRecursivity {

    public static void main(String[] args) {
        TryingRecursivity program = new TryingRecursivity();
        program.startTrial();
    }

    /**
     * Metodo de inicio del programa de pruebas.
     */
    private void startTrial() {
        boolean trialOver;
        do {
            trialOver = showTrialMenu();
        } while (!trialOver);
    }

    /**
     * Metodo que muestra el menu con las opciones el programa entero.
     *
     * @return un boolean el cual dice que termina el programa o inicia otro metodo
     */
    private boolean showTrialMenu() {
        switch (InputData.insertUserString("\nWelcome to the MENU!!!\n\n1.- Calcular  xn, x real i n sencer. No pots utilitzar Math.Pow.\n\n2.- Realitzar la divisió de dos números sencers positius mitjançant successives restes. Volem obtenir el quocient.\n\n3.- Donada una cadena de caràcters, mètode recursiu que inverteixi la cadena, no que la mostri invertida. Un cop executat el mètode recursiu s’obté una cadena igual que l’original però invertida.\n\n4.- Fer el producte dels elements d'un vector. Cal omplir i mostrar un vector de n elements amb valors aleatoris entre -10 i 10 i obtenir recursivament el producte.\n\n5.- Exit\n\nLets try option... ")) {
            case "1":
                recursivityPower();
                break;
            case "2":
                recursivityDivision();
                break;
            case "3":
                recursivityStringInversion();
                break;
            case "4":
                recursivityArrayNumsProduct();
                break;
            case "5":
                System.out.println("GOODBYE!!! :D");
                return true;
            default:
                System.out.println("That's not an option, Try again!");
        }
        return false;
    }

    /**
     * Metodo de tiempo de pausa para dejarle espacio al usuario.
     */
    private static void pause() {
        Scanner reader = new Scanner(System.in);
        System.out.print("Press enter to continue...");
        reader.nextLine();
    }

    /**
     * Metodo que inicia la prueba de los metodos de recursividad de la potencia.
     */
    private static void recursivityPower() {
        double base = InputData.askDouble("base");
        int exponent = InputData.askInt("exponent");
        System.out.println("The result of " + base + " to the power of " + exponent + " is " + RecursivityMethods.powerOf(base, exponent));
        pause();
    }

    /**
     * Metodo que inicia la prueba de los metodos de recursividad de la division.
     */
    private static void recursivityDivision() {
        int dividend = InputData.askInt("dividend"), divisor = InputData.askInt("divisor");
        System.out.println("The result of the division between " + dividend + " and " + divisor + " is " + RecursivityMethods.division(dividend, divisor, 1));
        pause();
    }

    /**
     * Metodo que inicia la prueba de los metodos de recursividad de la inversion de strings.
     */
    private static void recursivityStringInversion() {
        char[] arrayChar = {'h', 'o', 'l', 'a'};
        System.out.println("\""+InputData.stringOf(arrayChar) + "\" is the word.\nIn reverse will be...\n\n\"" + InputData.stringOf(RecursivityMethods.invertString(arrayChar, arrayChar.length-1))+"\"\n");
        pause();
    }

    /**
     * Metodo que inicia la prueba de los metodos de recursividad del Producto de numeros de array.
     */
    private static void recursivityArrayNumsProduct() {
        int[] trialArray = new int[3];
        InputData.fillArray(trialArray, 3, 1);
        System.out.println("\n" + InputData.stringOf(trialArray));
        System.out.println("The result of the product of all numbers is: " + RecursivityMethods.productOfValuesOf(trialArray, 0));
        pause();
    }

}
