package com.itb.dades;

import java.util.Scanner;

/**
 * Clase de manejo de datos otorgados por el usuario.
 */
public class InputData {
    /**
     * Metodo que pide una cadena de texto al usuario.
     *
     * @param message mensaje a mostrar al usuario junto la peticion.
     * @return espera de respuesta del usuario.
     */
    public static String insertUserString(String message) {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);
        return reader.nextLine();
    }

    /**
     * Metodo que pide un valor entero al usuario.
     *
     * @param variableName el nombre de la variable a otorgar el entero.
     * @return devuelve un numero entero dado por el usuario.
     */
    public static int askInt(String variableName) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Please insert the number for the " + variableName + ": ");
        while (!reader.hasNextInt()) {
            System.out.print("That's not a whole number, please try again: ");
            reader.nextLine();
        }
        return reader.nextInt();
    }

    /**
     * Metodo que pide un numero al usuario.
     *
     * @param variableName el nombre de la variable a otorgar el numero.
     * @return devuelve un numero dado por el usuario.
     */
    public static double askDouble(String variableName) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Please insert the number for the " + variableName + ": ");
        while (!reader.hasNextDouble()) {
            System.out.print("That's not a number, please try again: ");
            reader.nextLine();
        }
        return reader.nextDouble();
    }

    /**
     * Metodo de relleno de un array con valores aleatorios.
     *
     * @param array al cual se le otorgaran los numeros.
     * @param max   valor maximo de los numeros aleatorios a otorgar.
     * @param min   valor minimo de los numeros aleatorios a otorgar.
     */
    public static void fillArray(int[] array, int max, int min) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.floor(Math.random() * (max - min + 1) + min);
        }
    }

    /**
     * Transforma un array en forma de String para poder mostrarlo.
     *
     * @param array el cual se transformara.
     * @return array en forma de String.
     */
    public static String stringOf(int[] array) {
        StringBuilder arrayString = new StringBuilder();
        for (int values : array) {
            arrayString.append("[");
            arrayString.append(values);
            arrayString.append("] ");
        }
        return arrayString.toString();
    }

    /**
     * Transforma un array en forma de String para poder mostrarlo.
     *
     * @param array el cual se transformara.
     * @return array en forma de String.
     */
    public static String stringOf(char[] array) {
        StringBuilder arrayString = new StringBuilder();
        for (char values : array) arrayString.append(values);
        return arrayString.toString();
    }

}
