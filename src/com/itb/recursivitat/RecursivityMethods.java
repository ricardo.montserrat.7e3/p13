package com.itb.recursivitat;

/**
 * Clase de los metodos de recursividad a probar.
 */
public class RecursivityMethods {
    /**
     * Metodo recursivo de potencia.
     *
     * @param base     de la potencia.
     * @param exponent al cual se elevara la potencia.
     * @return devuelve el numero resultado de la operacion.
     */
    public static double powerOf(double base, int exponent) {
        if (exponent > 0) {
            return base * powerOf(base, exponent - 1);
        }
        if (exponent < 0) {
            return base * powerOf(base, exponent + 1); 
        }
        return 1;
    }

    /**
     * Metodo recursivo de division
     *
     * @param dividend         numero a ser dividido.
     * @param divisor          numero a dividir otro numero.
     * @param timesSustraction contador para numero de veces que se hace una resta.
     * @return devuelve un numero entero resultado de la division.
     */
    public static int division(int dividend, int divisor, int timesSustraction) {
        if (dividend > divisor) return division(dividend - divisor, divisor, timesSustraction + 1);
        return timesSustraction;
    }

    /**
     * Invierte  un Array de caracteres.
     * @param toInvert array a invertir.
     * @param position posicion inicial para recursividad.
     * @return un array de caracteres invertido.
     */
    public static char[] invertString(char[] toInvert, int position) {
        int swap = toInvert.length-1 - position;
        if (swap < position) {
            char temp = toInvert[swap];
            toInvert[swap] = toInvert[position];
            toInvert[position] = temp;
            return invertString(toInvert, --position);
        } else {
            return toInvert;
        }
    }

    /**
     * Metodo que devuelve el producto de todos los valores dentro de un array de manera recursiva.
     *
     * @param array           del cual se tomaran los valores a multiplicar.
     * @param initialPosition posicion inicial en el array al empezar a contar.
     * @return el resultado del producto de todos los numeros dentro del array.
     */
    public static int productOfValuesOf(int[] array, int initialPosition) {
        if (initialPosition + 1 <= array.length)
            return array[initialPosition] * productOfValuesOf(array, initialPosition + 1);
        return 1;
    }
}
